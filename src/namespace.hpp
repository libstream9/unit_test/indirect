#ifndef STREAM9_INDIRECT_TEST_SRC_NAMESPACE_HPP
#define STREAM9_INDIRECT_TEST_SRC_NAMESPACE_HPP

namespace std::ranges {}

namespace stream9::iterators {}
namespace stream9::ranges::views {}

namespace testing {

namespace st9 = stream9;

namespace iter { using namespace st9::iterators; }
namespace rng { using namespace std::ranges; }
namespace rng::views { using namespace st9::ranges::views; }

} // namespace testing

#endif // STREAM9_INDIRECT_TEST_SRC_NAMESPACE_HPP
