#include <stream9/indirect.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/indirect.hpp>
#include <stream9/less.hpp>
#include <stream9/optional.hpp>

namespace testing {

using stream9::indirect;
using stream9::less;
using stream9::opt;

BOOST_AUTO_TEST_SUITE(indirect_)

    BOOST_AUTO_TEST_CASE(opt_)
    {
        opt<int> i1 = 1;
        opt<int> i2 = 2;
        opt<int> i3 = 3;
        indirect<less> lt;

        BOOST_TEST(lt(i1, i2));
        BOOST_TEST(lt(i2, i3));
    }

BOOST_AUTO_TEST_SUITE_END() // indirect_

} // namespace testing
