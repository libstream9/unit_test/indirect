#include <stream9/indirect/indirect_range.hpp>

#include "namespace.hpp"

#include <memory>
#include <functional>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(indirect_range_)

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::vector<int*> v;

        auto r = rng::views::indirect(v);

        using R = decltype(r);

        static_assert(std::same_as<rng::range_value_t<R>, int>);
        static_assert(std::same_as<rng::range_reference_t<R>, int&>);
        static_assert(std::same_as<rng::range_rvalue_reference_t<R>, int&&>);
    }

BOOST_AUTO_TEST_SUITE_END() // indirect_range_

BOOST_AUTO_TEST_SUITE(const_indirect_range_)

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::vector<int*> v;

        auto r = rng::views::const_indirect(v);

        using R = decltype(r);

        static_assert(std::same_as<rng::range_value_t<R>, int>);
        static_assert(std::same_as<rng::range_reference_t<R>, int const&>);
        static_assert(std::same_as<rng::range_rvalue_reference_t<R>, int const&&>);
    }

BOOST_AUTO_TEST_SUITE_END() // const_indirect_range_

} // namespace testing
