#include <stream9/indirect/redirect.hpp>

#include "namespace.hpp"

#include <functional>
#include <memory>
#include <optional>

#include <boost/test/unit_test.hpp>

#include <stream9/tag_invoke.hpp>

namespace testing {

using stream9::redirect;
using stream9::redirect_result_t;

BOOST_AUTO_TEST_SUITE(redirect_)

    BOOST_AUTO_TEST_CASE(raw_pointer_)
    {
        int v = 1;

        BOOST_TEST(redirect(&v) == 1);

        static_assert(std::same_as<redirect_result_t<int*>, int&>);
    }

    BOOST_AUTO_TEST_CASE(unique_ptr_)
    {
        auto v = std::make_unique<int>(1);

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(shared_ptr_)
    {
        auto v = std::make_shared<int>(1);

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(reference_wrapper_)
    {
        int i = 1;
        auto v = std::ref(i);

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(input_iterator_)
    {
        std::vector<int> x { 1, 2, 3 };
        auto v = x.begin();

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(output_iterator_)
    {
        std::vector<int> x { 1, 2, 3 };
        auto v = x.begin();

        redirect(v) = 0;

        BOOST_TEST(redirect(v) == 0);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(optional_)
    {
        std::optional<int> v = 1;

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int&>);
    }

    BOOST_AUTO_TEST_CASE(optional_const_)
    {
        std::optional<int> const v = 1;

        BOOST_TEST(redirect(v) == 1);

        using I = decltype(v);

        static_assert(std::same_as<redirect_result_t<I>, int const&>);
    }

BOOST_AUTO_TEST_SUITE_END() // redirect_

} // namespace testing
