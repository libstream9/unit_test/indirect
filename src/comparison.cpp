#include <stream9/indirect/comparison.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/tag_invoke.hpp>

namespace testing {

namespace st9 = stream9;

BOOST_AUTO_TEST_SUITE(indirect_)

    BOOST_AUTO_TEST_CASE(equal_to_)
    {
        int a = 1;
        int b = 1;

        st9::indirect::equal_to f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(f(&a, &a));
        BOOST_TEST(f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(not_equal_to_)
    {
        int a = 1;
        int b = 2;

        st9::indirect::not_equal_to f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(!f(&a, &a));
        BOOST_TEST(f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(less_)
    {
        int a = 1;
        int b = 2;

        st9::indirect::less f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(!f(&a, &a));
        BOOST_TEST(!f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(greater_)
    {
        int a = 2;
        int b = 1;

        st9::indirect::greater f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(!f(&a, &a));
        BOOST_TEST(!f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(less_equal_)
    {
        int a = 1;
        int b = 2;

        st9::indirect::less_equal f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(f(&a, &a));
        BOOST_TEST(!f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(greater_equal_)
    {
        int a = 2;
        int b = 1;

        st9::indirect::greater_equal f;

        BOOST_TEST(f(&a, &b));
        BOOST_TEST(f(&a, &a));
        BOOST_TEST(!f(&b, &a));
        static_assert(noexcept(f(&a, &b)));
    }

    BOOST_AUTO_TEST_CASE(compare_three_way_)
    {
        int a = 1;
        int b = 2;

        st9::indirect::compare_three_way f;

        BOOST_CHECK(f(&a, &b) == std::strong_ordering::less);
        BOOST_CHECK(f(&a, &a) == std::strong_ordering::equal);
        BOOST_CHECK(f(&b, &a) == std::strong_ordering::greater);
        static_assert(noexcept(f(&a, &b)));
    }

BOOST_AUTO_TEST_SUITE_END() // indirect_

} // namespace testing
