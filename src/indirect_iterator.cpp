#include <stream9/indirect/indirect_iterator.hpp>

#include "namespace.hpp"

#include <memory>
#include <functional>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(indirect_iterator_)

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::vector<int*> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_pointer_)
    {
        std::vector<int const*> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(unique_ptr_)
    {
        std::vector<std::unique_ptr<int>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_unique_ptr_)
    {
        std::vector<std::unique_ptr<int const>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(shared_ptr_)
    {
        std::vector<std::shared_ptr<int>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_shared_ptr_)
    {
        std::vector<std::shared_ptr<int const>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(reference_wrapper_)
    {
        std::vector<std::reference_wrapper<int>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_reference_wrapper_)
    {
        std::vector<std::reference_wrapper<int const>> v;

        iter::indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        using I1 = std::vector<int>::iterator;
        std::vector<I1> v;

        iter::indirect_iterator it { v.begin() };

        using I2 = decltype(it);

        static_assert(std::random_access_iterator<I2>);
        static_assert(std::same_as<std::iter_value_t<I2>, int>);
        static_assert(std::same_as<std::iter_reference_t<I2>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I2>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_iterator_)
    {
        using I1 = std::vector<int>::const_iterator;
        std::vector<I1> v;

        iter::indirect_iterator it { v.begin() };

        using I2 = decltype(it);

        static_assert(std::random_access_iterator<I2>);
        static_assert(std::same_as<std::iter_value_t<I2>, int>);
        static_assert(std::same_as<std::iter_reference_t<I2>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I2>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_convertible_base_iterator_)
    {
        using V = std::vector<int*>;

        static_assert(std::convertible_to<V::iterator, V::const_iterator>);

        using I = iter::indirect_iterator<V::const_iterator>;

        I it { V::iterator{} };

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

BOOST_AUTO_TEST_SUITE_END() // indirect_iterator_

BOOST_AUTO_TEST_SUITE(const_indirect_iterator_)

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::vector<int*> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_pointer_)
    {
        std::vector<int const*> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(unique_ptr_)
    {
        std::vector<std::unique_ptr<int>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_unique_ptr_)
    {
        std::vector<std::unique_ptr<int const>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(shared_ptr_)
    {
        std::vector<std::shared_ptr<int>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_shared_ptr_)
    {
        std::vector<std::shared_ptr<int const>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(reference_wrapper_)
    {
        std::vector<std::reference_wrapper<int>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_reference_wrapper_)
    {
        std::vector<std::reference_wrapper<int const>> v;

        iter::const_indirect_iterator it { v.begin() };

        using I = decltype(it);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        using I1 = std::vector<int>::iterator;
        std::vector<I1> v;

        iter::const_indirect_iterator it { v.begin() };

        using I2 = decltype(it);

        static_assert(std::random_access_iterator<I2>);
        static_assert(std::same_as<std::iter_value_t<I2>, int>);
        static_assert(std::same_as<std::iter_reference_t<I2>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I2>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(const_iterator_)
    {
        using I1 = std::vector<int>::const_iterator;
        std::vector<I1> v;

        iter::const_indirect_iterator it { v.begin() };

        using I2 = decltype(it);

        static_assert(std::random_access_iterator<I2>);
        static_assert(std::same_as<std::iter_value_t<I2>, int>);
        static_assert(std::same_as<std::iter_reference_t<I2>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I2>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_const_iterator_)
    {
        std::vector<int*> v;

        iter::const_indirect_iterator it1 { v.begin() };
        iter::const_indirect_iterator<std::vector<int*>::const_iterator> it2 { it1 };

        using I = decltype(it2);

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_convertible_base_iterator_)
    {
        using V = std::vector<int*>;

        static_assert(std::convertible_to<V::iterator, V::const_iterator>);

        using I = iter::const_indirect_iterator<V::const_iterator>;

        I it { V::iterator{} };

        static_assert(std::random_access_iterator<I>);
        static_assert(std::same_as<std::iter_value_t<I>, int>);
        static_assert(std::same_as<std::iter_reference_t<I>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_non_const_indirect_iterator_with_convertible_base_iterator_1_)
    {
        using V = std::vector<int*>;

        static_assert(std::convertible_to<V::iterator, V::const_iterator>);

        using I1 = iter::const_indirect_iterator<V::const_iterator>;
        using I2 = iter::indirect_iterator<V::iterator>;

        I1 it { I2{} };

        static_assert(std::random_access_iterator<I1>);
        static_assert(std::same_as<std::iter_value_t<I1>, int>);
        static_assert(std::same_as<std::iter_reference_t<I1>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I1>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_indirect_iterator_with_convertible_base_iterator_2_)
    {
        using V = std::vector<int*>;

        static_assert(std::convertible_to<V::iterator, V::const_iterator>);

        using I1 = iter::const_indirect_iterator<V::const_iterator>;
        using I2 = iter::indirect_iterator<V::const_iterator>;

        I1 it { I2{} };

        static_assert(std::random_access_iterator<I1>);
        static_assert(std::same_as<std::iter_value_t<I1>, int>);
        static_assert(std::same_as<std::iter_reference_t<I1>, int const&>);
        static_assert(std::same_as<std::iter_difference_t<I1>, std::ptrdiff_t>);
    }

    BOOST_AUTO_TEST_CASE(compare_with_indirect_iterator_)
    {
        using V = std::vector<int*>;

        using I1 = iter::const_indirect_iterator<V::const_iterator>;
        using I2 = iter::indirect_iterator<V::const_iterator>;

        V v;
        I1 i1 { v.begin() };
        I2 i2 { v.begin() };

        BOOST_CHECK(i1 == i2);
        BOOST_CHECK(i1 <=> i2 == std::strong_ordering::equal);
    }

BOOST_AUTO_TEST_SUITE_END() // const_indirect_iterator_

} // namespace testing

